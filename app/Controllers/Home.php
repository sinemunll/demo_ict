<?php

namespace App\Controllers;

use App\Models\Brand;
use App\Models\BrandModel;
use App\Models\Customer;
use App\Models\RepairLocation;
use App\Models\RepairLocationType;
use App\Models\RepairType;
use App\Models\Service;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\Request;


class Home extends BaseController
{
    use ResponseTrait;

    private $brand;
    private $brandModel;
    private $repairType;
    private $repairLocation;
    private $repairLocationType;

    public function __construct()
    {
        $this->brand = new Brand();
        $this->brandModel = new BrandModel();
        $this->repairType = new RepairType();
        $this->repairLocation = new RepairLocation();
        $this->repairLocationType = new RepairLocationType();
    }

    public function index()
    {

        $data = [
            'data' => $this->brand->select('id,name')->findAll(),
            'type' => $this->repairType->select('id,name')->findAll()
        ];

        return view('home', $data);

    }

    public function model($id)
    {

        $builder = $this->brandModel->select('name,id');
        $data = $builder->where('brand_id', $id)->findAll();
        return $this->respond($data);


    }

    public function arrayPluck($array, $key) {
        return array_map(function($v) use ($key) {
            return is_object($v) ? $v->$key : $v[$key];
        }, $array);
    }

    public function location($id)
    {

        $builder = $this->repairLocationType->select('repairLocation_id')->where('repairType_id', $id)->findAll();
        $repairLocation=$this->arrayPluck($builder,'repairLocation_id');
        $data=$this->repairLocation->select('name,id')->whereIn('id',$repairLocation)->findAll();
        return $this->respond($data);


    }
    public function services(){



            $brand_id = $this->request->getPost('brand_id');
            $model_id = $this->request->getPost('model_id');
            $customer = $this->request->getPost('customer_id');
            $repair_type = $this->request->getPost('repair_type');
            $repair_location = $this->request->getPost('repair_location');
            $repair_date = $this->request->getPost('repair_date');
            $repair_time = $this->request->getPost('repair_time');


            $customerModel = model('Customer');
            $cust = $customerModel->like('name', $customer)->first();
            if ($cust) {
                $customer = $cust->id;
            } else {
                $cust_model = new Customer();
                $cust_model->save([
                    'name' => $customer,
                    'created_date' => date('Y-m-d H:i:s'),
                    'updated_date' => date('Y-m-d H:i:s')
                ]);
                $cust = $cust_model->orderBy('id', 'desc')->first();
                $customer = $cust->id;
            }


            $model = new Service();

            $model->save([
                'brand_id' => $brand_id,
                'model_id' => $model_id,
                'customer' => $customer,
                'repair_type' => $repair_type,
                'repair_location' => $repair_location,
                'repair_date' => $repair_date,
                'repair_time' => $repair_time
            ]);


            return true;


    }


}
