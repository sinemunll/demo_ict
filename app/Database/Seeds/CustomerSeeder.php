<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CustomerSeeder extends Seeder
{
    public function run()
    {
        $model = model('Customer');

        foreach ($this->dummyData() as $key) {
            $model->insert($key);
        }

    }

    /**
     * @return array[]
     */
    public function dummyData(): array
    {
        $date=date('Y-m-d H:i:s');
        return [
            1 => [
                'name' => 'Ayşe Yılmaz',
                'phone' =>'05555555555',
                'created_date' => $date,
                'updated_date' => $date,

            ],
            2 => [
                'name' => 'Ali Demir',
                'phone' =>'05454555455',
                'created_date' => $date,
                'updated_date' => $date,

            ],
            3 => [
                'name' => 'Elif Güven',
                'phone' =>'05057575555',
                'created_date' => $date,
                'updated_date' => $date,

            ]


        ];
    }
}
