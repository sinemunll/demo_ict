<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RepairTypeSeeder extends Seeder
{


    public function run()
    {
        $model = model('RepairType');
        foreach ($this->dummyData() as $key) {
            $model->insert($key);

        }

    }

    /**
     * @return array[]
     */
    public function dummyData(): array
    {
        $date=date('Y-m-d H:i:s');
        return [
            1 => [
                'name' => 'Oto Mekanik ve Motor Tamiri',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            2 => [
                'name' => 'Oto Elektrik Tamiri',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            3 => [
                'name' => 'Oto Kaporta',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            4 => [
                'name' => 'Oto Boya',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            5 => [
                'name' => 'Şase, Traverse Tamiri',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            6 => [
                'name' => 'Rot Balans Tamiri',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],


        ];
    }
}
