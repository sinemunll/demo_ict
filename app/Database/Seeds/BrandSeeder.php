<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use CodeIgniter\I18n\Time;

class BrandSeeder extends Seeder
{
    public function run()
    {
        $model = model('Brand');
        $brandModel = model('BrandModel');
        foreach ($this->dummyData() as $key) {
          $model->insert($key);
            foreach ($key['model'] as $attribute) {
                $brandModel->insert($this->createModelData($attribute, $model->insertID));

            }

        }

    }

    /**
     * @return array[]
     */
    public function dummyData(): array
    {
        $date=date('Y-m-d H:i:s');
        return [
            1 => [
                'name' => 'Audi',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,
                'model' => [
                    1 => [
                        'name' => 'A3',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],

                    2 => [
                        'name' => 'A4',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ]

                ]
            ],
            2 => [
                'name' => 'Ford',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,
                'model' => [
                    1 => [
                        'name' => 'Focus',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],

                    2 => [
                        'name' => 'Fiesta',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],
                    3 => [
                        'name' => 'Transit',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ]

                ]
            ],
            3 => [
                'name' => 'Citroen',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,
                'model' => [
                    1 => [
                        'name' => 'Berlingo',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],

                    2 => [
                        'name' => 'Nemo',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],
                    3 => [
                        'name' => 'C3',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],
                    4 => [
                        'name' => 'C4',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ]

                ]
            ],
            4 => [
                'name' => 'Chevrolet',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,
                'model' => [
                    1 => [
                        'name' => 'Aveo',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],

                    2 => [
                        'name' => 'Captiva',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],


                ]
            ],
            5 => [
                'name' => 'Mercedes-Benz',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,
                'model' => [
                    1 => [
                        'name' => 'B Serisi',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],

                    2 => [
                        'name' => 'C Serisi',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],
                    3 => [
                        'name' => 'CLA Serisi',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],


                ]
            ],
            6 => [
                'name' => 'Toyota',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,
                'model' => [
                    1 => [
                        'name' => 'Auris',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],

                    2 => [
                        'name' => 'Corolla',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],
                    3 => [
                        'name' => 'Hilux',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],
                    4 => [
                        'name' => 'Yaris',
                        'status' => 1,
                        'created_date' => $date,
                        'updated_date' => $date,
                    ],

                ]
            ]

        ];
    }


    /**
     * @param array $attribute
     * @param int $brandId
     * @return array
     */
    private function createModelData(array $attribute, int $brandId)
    {
        return [
            'name' => $attribute['name'],
            'status' => $attribute['status'],
            'created_date' => $attribute['created_date'],
            'updated_date' => $attribute['updated_date'],
            'brand_id' => $brandId,

        ];
    }
}
