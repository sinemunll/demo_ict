<?php

namespace App\Database\Seeds;

use App\Models\RepairLocation;
use App\Models\RepairLocationType;
use App\Models\RepairType;
use CodeIgniter\Database\BaseConnection;
use CodeIgniter\Database\Seeder;
use Config\Database;

class RepairLocationTypeSeeder extends Seeder
{

    private $repairType;
    private $repairLocation;

    public function __construct(Database $config, BaseConnection $db = null)
    {
        parent::__construct($config, $db);
        $this->repairType = new RepairType();
        $this->repairLocation = new RepairLocation();
    }

    public function run()
    {
        $model = model('RepairLocationType');
        $date=date('Y-m-d H:i:s');
        for ($i = 1; $i < 10; $i++) {
            $repair_type = $this->repairType->select('id')->orderBy('id', 'random')->first();
            $repair_location = $this->repairLocation->select('id')->orderBy('id', 'random')->first();

            $data = [
                'repairLocation_id' => $repair_location['id'],
                'repairType_id' => $repair_type['id'],
                'created_date' => $date,
                'updated_date' => $date,
            ];
            $model->insert($data);
        }


    }


}
