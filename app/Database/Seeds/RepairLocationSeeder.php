<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RepairLocationSeeder extends Seeder
{
    public function run()
    {
        $model = model('RepairLocation');
        foreach ($this->dummyData() as $key) {
            $model->insert($key);

        }

    }

    /**
     * @return array[]
     */
    public function dummyData(): array
    {
        $date=date('Y-m-d H:i:s');
        return [
            1 => [
                'name' => 'Bahçelievler/İstanbul',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            2 => [
                'name' => 'Gaziosmanpaşa/İstanbul',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            3 => [
                'name' => 'Ümraniye/İstanbul',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            4 => [
                'name' => 'Şişli/İstanbul',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            5 => [
                'name' => 'Sarıyer/İstanbul',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            6 => [
                'name' => 'Pendik/İstanbul',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            7 => [
                'name' => 'Başakşehir/İstanbul',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            8 => [
                'name' => 'Esenyurt/İstanbul',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],
            9 => [
                'name' => 'Üsküdar/İstanbul',
                'status' => 1,
                'created_date' => $date,
                'updated_date' => $date,

            ],


        ];
    }
}
