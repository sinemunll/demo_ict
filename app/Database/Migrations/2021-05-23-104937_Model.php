<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Model extends Migration
{
	public function up()
	{
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'brand_id'       => [
                'type'       => 'INT',
                'unsigned' => true
            ],
            'status'       => [
                'type'       => 'INT',
            ],
            'created_date datetime default current_timestamp',
            'updated_date datetime default current_timestamp on update current_timestamp',
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('brand_id', 'brands','id');
        $this->forge->createTable('brand_models');
	}

	public function down()
	{
        $this->forge->dropTable('brand_models');
	}
}
