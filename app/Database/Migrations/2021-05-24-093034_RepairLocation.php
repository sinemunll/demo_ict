<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RepairLocation extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'status'       => [
                'type'       => 'INT',
            ],
            'created_date datetime default current_timestamp',
            'updated_date datetime default current_timestamp on update current_timestamp',
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('repair_locations');
    }

    public function down()
    {
        $this->forge->dropTable('repair_locations');
    }
}
