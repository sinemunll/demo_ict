<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RepairTypeLocation extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'repairType_id'       => [
                'type'           => 'INT',

            ],
            'repairLocation_id'       => [
                'type'       => 'INT',
            ],
            'created_date datetime default current_timestamp',
            'updated_date datetime default current_timestamp on update current_timestamp',
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('repair_location_type');
    }


    public function down()
    {
        $this->forge->dropTable('repair_location_type');
    }
}
