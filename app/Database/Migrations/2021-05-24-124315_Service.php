<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Service extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'brand_id'       => [
                'type'       => 'INT',
                'unsigned' => true
            ],
            'model_id'       => [
                'type'       => 'INT',
                'unsigned' => true
            ],
            'customer_id'       => [
                'type'       => 'INT',
                'unsigned' => true
            ],
            'repair_type_id'       => [
                'type'       => 'INT',
                'unsigned' => true
            ],
            'repair_location_id'       => [
                'type'       => 'INT',
                'unsigned' => true
            ],
            'repair_date'       => [
                'type'       => 'DATE',
            ],
            'repair_TİME'       => [
                'type'       => 'TIME',
            ],
            'created_date datetime default current_timestamp',
            'updated_date datetime default current_timestamp on update current_timestamp',
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('brand_id', 'brands','id');
        $this->forge->addForeignKey('model_id', 'brand_models','id');
        $this->forge->addForeignKey('customer_id', 'customers','id');
        $this->forge->addForeignKey('repair_type_id', 'repair_types','id');
        $this->forge->addForeignKey('repair_location_id', 'repair_locations','id');
        $this->forge->createTable('services');
    }

    public function down()
    {
        $this->forge->dropTable('services');
    }
}
