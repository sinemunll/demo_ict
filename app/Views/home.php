<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Yeni Araç Girişi
            </div>
            <div class="card-body">
                <form action="#" id="form" class="form-horizontal">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Marka</label>
                            <select name="brand_id" class="custom-select" id="brand">
                                <option selected>Seçiniz</option>
                                <?php foreach ($data as $item): ?>
                                    <option value="<?php echo $item['id'] ?>"><?php echo $item['name'] ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Model</label>
                            <select name="model" id="model" class="custom-select "></select>

                        </div>
                    </div>
                    <div class="form-group">
                        <label >Müşteri</label>
                        <input type="text" class="form-control" name="customer" id="customer"  placeholder="Lütfen müşteriyi giriniz">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Tamir Türü</label>
                            <select name="repair_type" class="custom-select" id="repair_type">
                                <option selected>Seçiniz</option>
                                <?php foreach ($type as $item): ?>
                                    <option value="<?php echo $item['id'] ?>"><?php echo $item['name'] ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label >Tamir Yeri</label>
                            <select name="repair_location" id="repair_location" class="custom-select "></select>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Tamir Tarihi</label>
                            <input type="date" class="form-control" name="repair_date" id="repair_date"  placeholder="Lütfen tarih seçiniz">
                        </div>
                        <div class="form-group col-md-6">
                            <label >Tamir Saati</label>
                            <input type="time" class="form-control" name="repair_time" id="repair_time"  placeholder="Lütfen tarih seçiniz">

                        </div>
                    </div>

                                <button type="button" id="save" class="btn btn-primary btn-block">Kaydet</button>
                </form>
            </div>
        </div>
    </div>

</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
        integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF"
        crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#brand,#repair_type').change(function () {

            var id=$(this).attr('id');
            if(id=='brand'){
                var brand = $('#brand').val();
                $.getJSON("<?php echo base_url();?>/brand/model/json/"+brand, function (jsonResult) {
                    $('#model').empty();
                    $('#model').append(
                        $("<option></option>").text('Seçiniz').val('')
                    );

                    $.each(jsonResult, function(i, item) {
                        $('#model').append(
                            $("<option></option>").text(this.name).val(this.id)
                        );
                    });
                });
            }else{
                var repair_type = $('#repair_type').val();
                $.getJSON("<?php echo base_url();?>/repair/type/location/"+repair_type, function (jsonResult) {
                    $('#repair_location').empty();
                    $('#repair_location').append(
                        $("<option></option>").text('Seçiniz').val('')
                    );
                    $.each(jsonResult, function(i, item) {
                        $('#repair_location').append(
                            $("<option></option>").text(this.name).val(this.id)
                        );
                    });
                });
            }



        })

        $('#save').click(function () {
            $.ajax({

                url: "<?php echo base_url();?>/services/create/",
                method: 'POST',
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                data: {
                    'brand_id': $('#brand').val(),
                    'model_id': $('#model').val(),
                    'customer_id': $('#customer').val(),
                    'repair_type_id': $('#repair_type').val(),
                    'repair_location': $('#repair_location').val(),
                    'repair_date': $('#repair_date').val(),
                    'repair_time': $('#repair_time').val(),
                    '_token': '{{ csrf_token() }}'
                },

                success: function (result) {
                    console.log(result)
                    alert('Başarılı')


                }

            });

        });
    })
</script>
</body>
</html>
